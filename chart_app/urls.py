from django.urls import path
from export_csv_app import views
from chart_app import views

urlpatterns = [
    path('profile/', views.profile_chart, name='profile_chart'),
    path('follower-count/', views.follower_count_chart, name='follower_count_chart'),
    path('profile-views/', views.profile_views_count_chart, name='profile_views_count_chart'),
    path('reach/', views.reach, name='reach'),
    path('tap-website/', views.tap_website, name='tap-website'),
    path('tap-direction/', views.tap_direction, name='tap-direction'),
    path('tap-email/', views.tap_email, name='tap-email'),
    path('tap-message/', views.tap_message, name='tap-message'),
    path('tap-call_count/', views.tap_call_count, name='tap-call_count'),


]


