from django.shortcuts import render
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from core.models import Profile , Feed , Stories
# Create your views here.


@login_required()
def profile_chart(request, pageId):
    return render(request, 'profile_chart.html' , { 'pageId': pageId })

@login_required
def follower_count_chart(request, pageId):
    labels=[]
    data = []


    profile = Profile.objects.filter(pageId=pageId)[:7]
    for p in profile:
        data.append(p.follower_count)
        labels.append(p.timestamp.date())
    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })


@login_required
def profile_views_count_chart(request, pageId):
    labels=[]
    data = []


    profile = Profile.objects.filter(pageId=pageId)[:7]
    for p in profile:
        data.append(p.profile_views)
        labels.append(p.timestamp.date())
    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })





@login_required
def reach(request, pageId):
    labels=[]
    data = []


    profile = Profile.objects.filter(pageId=pageId)[:7]
    for p in profile:
        data.append(p.reach)
        labels.append(p.timestamp.date())
    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })

@login_required
def tap_website(request, pageId):
    labels=[]
    data = []


    profile = Profile.objects.filter(pageId=pageId)[:7]
    for p in profile:
        data.append(p.tap_website)
        labels.append(p.timestamp.date())
    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })


def tap_direction(request, pageId):
    labels=[]
    data = []


    profile = Profile.objects.filter(pageId=pageId)[:7]
    for p in profile:
        data.append(p.tap_direction)
        labels.append(p.timestamp.date())
    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })


def tap_email(request, pageId):
    labels=[]
    data = []


    profile = Profile.objects.filter(pageId=pageId)[:7]
    for p in profile:
        data.append(p.tap_email)
        labels.append(p.timestamp.date())
    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })


def tap_message(request, pageId):
    labels=[]
    data = []


    profile = Profile.objects.filter(pageId=pageId)[:7]
    for p in profile:
        data.append(p.tap_message)
        labels.append(p.timestamp.date())
    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })



def tap_call_count(request, pageId):
    labels=[]
    data = []


    profile = Profile.objects.filter(pageId=pageId)[:7]
    for p in profile:
        data.append(p.tap_call_count)
        labels.append(p.timestamp.date())
    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })


