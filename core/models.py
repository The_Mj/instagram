from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()
# Create your models here.
class Profile(models.Model):
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    pageId = models.CharField(max_length=128)
    cities = models.CharField(max_length=100 ,null=True)
    countries = models.CharField(max_length=100 ,null=True)
    gender_age = models.CharField(max_length=100 ,null=True)
    tap_email = models.IntegerField()
    follower_count = models.IntegerField()
    tap_direction = models.IntegerField()
    tap_call_count = models.IntegerField()
    tap_message = models.IntegerField()
    tap_website = models.IntegerField()
    impressions = models.CharField(max_length=100 ,null=True)
    reach = models.CharField(max_length=100 ,null=True)
    profile_views = models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)



class Feed(models.Model):
    TYPE_CHOICES = (
        ('image', 'تصویر'),
        ('video', 'ویدیو'),
        ('carousel', 'اسلاید'),
    )
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    pageId = models.CharField(max_length=128)
    url = models.URLField()
    caption = models.CharField(max_length=2300)
    type_model = models.CharField(choices=TYPE_CHOICES,default='image',blank=True, max_length=8)
    comment_count = models.IntegerField()
    like_count = models.IntegerField()
    saved_count = models.IntegerField(null=True , default=None)
    video_views_count = models.IntegerField(null=True)
    impressions = models.CharField(max_length=100 ,null=True , default=None)
    reach = models.CharField(max_length=100 ,null=True , default=None)
    date = models.DateTimeField()
    media_id=models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)
    thumbnail_url = models.URLField(max_length=500 , null=True, default=None)


class Stories(models.Model):
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    pageId = models.CharField(max_length=128)
    date = models.DateTimeField()
    impressions = models.CharField(max_length=100 ,null=True)
    reach = models.CharField(max_length=100 ,null=True)
    exit_count = models.IntegerField()
    reply_count = models.IntegerField()
    tap_forward_count = models.IntegerField()
    tap_back_count = models.IntegerField()
    story_id=models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)




