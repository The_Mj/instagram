# Generated by Django 3.1 on 2020-09-26 09:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Feed',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pageId', models.CharField(max_length=128)),
                ('url', models.URLField()),
                ('caption', models.CharField(max_length=2300)),
                ('type_model', models.CharField(blank=True, choices=[('image', 'تصویر'), ('video', 'ویدیو'), ('carousel', 'اسلاید')], default='image', max_length=8)),
                ('comment_count', models.IntegerField()),
                ('like_count', models.IntegerField()),
                ('saved_count', models.IntegerField(default=None, null=True)),
                ('video_views_count', models.IntegerField(null=True)),
                ('impressions', models.CharField(default=None, max_length=100, null=True)),
                ('reach', models.CharField(default=None, max_length=100, null=True)),
                ('date', models.DateTimeField()),
                ('media_id', models.IntegerField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('thumbnail_url', models.URLField(max_length=2048)),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pageId', models.CharField(max_length=128)),
                ('cities', models.CharField(max_length=100, null=True)),
                ('countries', models.CharField(max_length=100, null=True)),
                ('gender_age', models.CharField(max_length=100, null=True)),
                ('tap_email', models.IntegerField()),
                ('follower_count', models.IntegerField()),
                ('tap_direction', models.IntegerField()),
                ('tap_call_count', models.IntegerField()),
                ('tap_message', models.IntegerField()),
                ('tap_website', models.IntegerField()),
                ('impressions', models.CharField(max_length=100, null=True)),
                ('reach', models.CharField(max_length=100, null=True)),
                ('profile_views', models.IntegerField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Stories',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pageId', models.CharField(max_length=128)),
                ('date', models.DateTimeField()),
                ('impressions', models.CharField(max_length=100, null=True)),
                ('reach', models.CharField(max_length=100, null=True)),
                ('exit_count', models.IntegerField()),
                ('reply_count', models.IntegerField()),
                ('tap_forward_count', models.IntegerField()),
                ('tap_back_count', models.IntegerField()),
                ('story_id', models.IntegerField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
