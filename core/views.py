# core/views.py

from django.contrib.auth import authenticate, login, logout
import requests
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse , HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core import serializers
import unicodedata
import json
from django.contrib.auth import get_user_model
User = get_user_model()
from core.models import Profile , Feed , Stories
import datetime


# Create your views here.
# def login(request):
#   return render(request, 'login2.html')


def home(request):
    # HttpResponse('salam')
  return render(request, 'home.html')


def index(request):




    print(datetime.datetime.now().strftime("%X"))
    userToken = 'EAAn2JFZAw9GEBAPrgUQZCcXFNstKpJqTLRrappr0fkqBbVqJUNJMFXUIewuhNaoZBZC6C5RuioGcyKHwAn0vv4l3Q9AGneeCncVDEZBTeI4GmVBGZCnjTq7bajx7DiCopbZBLdFt2CgiPZCZB3yI0y9ZCKZA33lvZAf9GVhJRO3ZCekNzv0y0JoMneG01DrWcDEwvcFsZD'

    userIdArr = []
    userTokenArr = []
    pageIdArr = []
    igbIdArr = []
    mediaArr = []
    storiesIdArr = []
    mediaIddict = {}
    storiesIddict = {}

    # user = User.objects.get(is_superuser=False)

    # for u in user:
    #     for i in u.social_auth.all():
    #         userIdArr.append(i.uid)
    #         userTokenArr.append(i.extra_data['access_token'])


    pageIdapi = requests.get('https://graph.facebook.com/v8.0/me?fields=id%2Cname%2Caccounts&access_token={}'.format(userToken))

    for i in pageIdapi.json()['accounts']['data']:
        pageIdArr.append(i['id'])

    # for userToken in userTokenArr:
    #     pageIdapi = requests.get('https://graph.facebook.com/v8.0/me?fields=id%2Cname%2Caccounts&access_token={}'.format(userToken))
    #     pageIdArr.append(pageIdapi.json()['accounts']['data'][0]['id'])

    for x in range(len(pageIdArr)):
        igbIdapi = requests.get('https://graph.facebook.com/v8.0/{}?fields=instagram_business_account&access_token={}'.format(pageIdArr[x],userToken))
        igbIdArr.append(igbIdapi.json()['instagram_business_account']['id'])
        r4 = requests.get('https://graph.facebook.com/v8.0/{}/insights?metric=audience_city%2Caudience_country%2Caudience_gender_age&period=lifetime&access_token={}'.format(igbIdArr[x], userToken))
        # get taps user
        r5 = requests.get('https://graph.facebook.com/v8.0/{}/insights?metric=email_contacts%2Cfollower_count%2Cget_directions_clicks%2Cphone_call_clicks%2Ctext_message_clicks%2Cwebsite_clicks&period=day&access_token={}'.format(igbIdArr[x], userToken))

        # get impressions user
        r6 = requests.get('https://graph.facebook.com/v8.0/{}/insights?metric=impressions%2Creach&period=day%2C%20week%2C%20days_28&access_token={}'.format(igbIdArr[x], userToken))
        # get profile views
        r7 = requests.get('https://graph.facebook.com/v8.0/{}/insights?metric=profile_views&period=day&access_token={}'.format(igbIdArr[x],userToken))

        p = Profile(pageId=pageIdArr[x], cities=r4.json()['data'][0]['values'][0]['value'],
                    countries=r4.json()['data'][1]['values'][0]['value'],
                    gender_age=r4.json()['data'][2]['values'][0]['value'],
                    tap_email=r5.json()['data'][0]['values'][0]['value'],
                    follower_count=r5.json()['data'][1]['values'][0]['value'],
                    tap_direction=r5.json()['data'][2]['values'][0]['value'],
                    tap_call_count=r5.json()['data'][3]['values'][0]['value'],
                    tap_message=r5.json()['data'][4]['values'][0]['value'],
                    tap_website=r5.json()['data'][5]['values'][0]['value'],
                    impressions=r6.json()['data'][0]['values'][0]['value'],
                    reach=r6.json()['data'][3]['values'][0]['value'],
                    profile_views=r7.json()['data'][0]['values'][0]['value'] )

        p.save()

    for y in range(len(igbIdArr)):
        mediaapi = requests.get("https://graph.facebook.com/v8.0/{}?fields=media&access_token={}".format(igbIdArr[y], userToken))
        # print(mediaapi.json()['media']['data'])

        mediaIddict[igbIdArr[y]] = []
        for j in mediaapi.json()['media']['data']:

            mediaIddict[igbIdArr[y]].append(j['id'])



        storiesapi = requests.get(
          "https://graph.facebook.com/v8.0/{}/stories?access_token={}".format(igbIdArr[y], userToken))


        storiesIddict[igbIdArr[y]] = []
        for f in storiesapi.json()['data']:

            storiesIddict[igbIdArr[y]].append(f['id'])




    for z in range(len(igbIdArr)):

        for w in range(len(mediaIddict[igbIdArr[z]])):
            r1 = requests.get('https://graph.facebook.com/v8.0/{}?fields=caption%2Ctimestamp%2Ccomments_count%2Clike_count%2Ccomments%2Cshortcode%2Cmedia_type%2Cthumbnail_url&access_token={}'.format(mediaIddict[igbIdArr[z]][w], userToken))
            # print(r1.json())
            # print(userToken)
            # print(mediaArr[z][w])
            # print(z , w)

            feed_url = 'https://www.instagram.com/p/{}/'.format(r1.json()['shortcode'])
            r2 = requests.get(
              'https://graph.facebook.com/v8.0/{}/insights?metric=impressions%2Creach%2Csaved&access_token={}'.format(
                mediaIddict[igbIdArr[z]][w], userToken))
            # print(r2.json()['data'])

            r3 = requests.get("https://graph.facebook.com/v8.0/{}/insights?metric=impressions%2Creach%2Csaved%2Cvideo_views&access_token={}".format(mediaIddict[igbIdArr[z]][w], userToken))
            if r1.json()['media_type'] == 'VIDEO':
                f2 = Feed(pageId=pageIdArr[z] ,url=feed_url, caption=r1.json()['caption'], type_model=r1.json()['media_type'], comment_count=r1.json()['comments_count'],
                        like_count=r1.json()['like_count'], saved_count=r2.json()['data'][2]['values'][0]['value'],
                        video_views_count=r3.json()['data'][3]['values'][0]['value'],
                        impressions=r2.json()['data'][0]['values'][0]['value'],
                        reach=r2.json()['data'][1]['values'][0]['value'], date=r1.json()['timestamp'] , media_id=mediaIddict[igbIdArr[z]][w], thumbnail_url=r1.json()['thumbnail_url'])
                f2.save()
            else:
                f1 = Feed(pageId=pageIdArr[z],url=feed_url, caption=r1.json()['caption'], type_model=r1.json()['media_type'], comment_count=r1.json()['comments_count'],
                        like_count=r1.json()['like_count'], saved_count=r2.json()['data'][2]['values'][0]['value'],
                        video_views_count=None, impressions=r2.json()['data'][0]['values'][0]['value'],
                        reach=r2.json()['data'][1]['values'][0]['value'], date=r1.json()['timestamp'],media_id=mediaIddict[igbIdArr[z]][w])
                f1.save()


    for h in range(len(igbIdArr)):
        for g in range(len(storiesIddict[igbIdArr[h]])):
            # get timestamp stories
            r8 = requests.get('https://graph.facebook.com/v8.0/{}?fields=timestamp&access_token={}'.format(storiesIddict[igbIdArr[h]][g],userTokenArr[h]))

            # get impressions , reach , exit , tap forward , tap back stories
            r9 = requests.get("https://graph.facebook.com/v8.0/{}/insights?metric=impressions%2Creach%2Cexits%2Ctaps_forward%2Ctaps_back%2Creplies&access_token={}".format(storiesIddict[igbIdArr[h]][g], userTokenArr[h]))
            s = Stories(pageId=pageIdArr[h],date=r8.json()['timestamp'], impressions=r9.json()['data'][0]['values'][0]['value'],
                        reach=r9.json()['data'][1]['values'][0]['value'],
                        exit_count=r9.json()['data'][2]['values'][0]['value'],
                        tap_forward_count=r9.json()['data'][3]['values'][0]['value'],
                        tap_back_count=r9.json()['data'][4]['values'][0]['value'],
                        reply_count=r9.json()['data'][5]['values'][0]['value'] , story_id=storiesIddict[igbIdArr[h]][g] )
            s.save()
    return render(request , 'home.html')

@login_required
def feed(request, pageId):
    feed = []
    print(pageId)

    feed.append(Feed.objects.filter(pageId=pageId))


    ctx={
        'feed': feed[0],
        'pageId':pageId,

    }
    return render(request, 'posts.html',ctx)
@login_required
def story(request, pageId):
    story = []
    for i in request.user.info_set.all():
        story.append(Stories.objects.filter(pageId=i.pageId))
        pageId = i.pageId

    ctx = {
        'story': story[0],
        'pageId': pageId,

    }

    return render(request,'stories.html',ctx)

@login_required
def profile(request, pageId):
    profile = []
    for i in request.user.info_set.all():
        profile.append(Profile.objects.filter(pageId=i.pageId))
        pageId = i.pageId

    ctx = {
        'profile': profile[0],
        'pageId': pageId,

    }


    return render(request , 'profile.html' , ctx)

# @login_required
# def profile_follower_count_chart(request):
#     labels=[]
#     data = []
#
#
#     profile = Profile.objects.filter(user=request.user)[:7]
#     for p in profile:
#         data.append(p.follower_count)
#         labels.append(p.timestamp)
#     return JsonResponse(data={
#         'labels': labels,
#         'data': data,
#     })
#
# @login_required()
# def chart(request, pageId):
#     return render(request, 'profile_chart.html')