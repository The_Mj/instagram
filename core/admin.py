from django.contrib import admin
from .models import Profile , Feed , Stories
# Register your models here.

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('pageId' , 'follower_count', 'id' )
    list_filter = ('pageId' , 'follower_count',)
    search_fields = ('user', )


@admin.register(Feed)
class FeedAdmin(admin.ModelAdmin):
    pass

@admin.register(Stories)
class StoryAdmin(admin.ModelAdmin):
    pass

