from django.shortcuts import render
from django.views.generic.base import TemplateView
import csv
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

# Create your views here.
from core.models import Feed , Profile , Stories
class CSVPageView(TemplateView):
    template_name = "csv_home.html"



# Simple CSV Write Operation
@login_required()
def csv_export_feed(request,pageId):
    # Create the HttpResponse object with the appropriate CSV header.
    # Get all data from UserDetail Databse Table
    feeds = Feed.objects.filter(pageId=pageId)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="csv_simple_write.csv"'

    writer = csv.writer(response)
    writer.writerow(['user', 'url', 'caption', 'type_media','comment_count','like_count','saved_count','video_views_count','impressions','reach','date','media_id',])
    for feed in feeds:
        writer.writerow([request.user, feed.url, feed.caption, feed.type_modelfeed.comment_count,feed.like_count,feed.saved_count,feed.video_views_count,feed.impressions,feed.reach,feed.date,feed.media_id,])

    # user
    # url
    # caption
    # type_model
    # comment_count
    # like_count
    # saved_count
    # video_views_count
    # impressions
    # reach
    # date
    # media_id
    return response

@login_required()
def csv_export_story(request,pageId):
    # Create the HttpResponse object with the appropriate CSV header.
    # Get all data from UserDetail Databse Table
    stories = Stories.objects.filter(pageId=pageId)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="csv_simple_write.csv"'

    writer = csv.writer(response)
    writer.writerow(['user''date''impressions''reach''exit_count''reply_count''tap_forward_count''tap_back_count''story_id'])
    for story in stories:
        writer.writerow([request.user,story.date,story.impressions,story.reach,story.exit_count,story.reply_count,story.tap_forward_count,story.tap_back_count,story.story_id,])

    # user
    # date
    # impressions
    # reach
    # exit_count
    # reply_count
    # tap_forward_count
    # tap_back_count
    # story_id

    return response




@login_required()
def csv_export_profile(request,pageId):
    print(pageId)
    # Create the HttpResponse object with the appropriate CSV header.
    # Get all data from UserDetail Databse Table
    profiles = Profile.objects.filter(pageId=pageId)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="csv_simple_write.csv"'

    writer = csv.writer(response)
    writer.writerow(['user','cities','countries','gender_age','tap_email','follower_count','tap_direction','tap_call_count','tap_message','tap_website','impressions','reach','profile_views',])
    for profile in profiles:
        writer.writerow([request.user,profile.cities,profile.countries,profile.gender_age,profile.tap_email,profile.follower_count,profile.tap_direction,profile.tap_call_count,profile.tap_message,profile.tap_website,profile.impressions,profile.reach,profile.profile_views,])

    # user
    # cities
    # countries
    # gender_age
    # tap_email
    # follower_count
    # tap_direction
    # tap_call_count
    # tap_message
    # tap_website
    # impressions
    # reach
    # profile_views

    return response