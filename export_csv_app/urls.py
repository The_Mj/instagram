from django.urls import path
from export_csv_app import views

urlpatterns = [
    path('', views.CSVPageView.as_view(), name='csv_home_page'),
    path('csv-export-feed/', views.csv_export_feed, name='csv_export_feed'),
    path('csv-export-profile/', views.csv_export_profile, name='csv_export_profile'),
    path('csv-export-story/', views.csv_export_story, name='csv_export_story'),
]