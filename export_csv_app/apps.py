from django.apps import AppConfig


class ExportCsvAppConfig(AppConfig):
    name = 'export_csv_app'
