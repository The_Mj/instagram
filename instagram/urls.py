"""instagram URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# social_app/urls.py

from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from core import views
from export_csv_app import urls as export_csv_app_urls
from chart_app import urls as chart_app_urls
from accounts import urls as accounts_app_urls
from django.conf import settings
urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/' , include(accounts_app_urls)),
    # path("login/", views.login, name="login"),
    # path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    # path('social-auth/', include(('social_django.urls', 'social_django'), namespace="social")),
    path('index/', views.index ,name="index"),
    path('<int:pageId>/feed/', views.feed ,name="index"),
    path('<int:pageId>/story/', views.story ,name="index"),
    path('<int:pageId>/profile/', views.profile ,name="index"),
    path("", views.home, name="home"),
    path('<int:pageId>/export/', include(export_csv_app_urls)),
    path('<int:pageId>/chart/', include(chart_app_urls)),

]




if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns