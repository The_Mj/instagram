"""instagram URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# social_app/urls.py

from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from accounts import views

urlpatterns = [

    path('signup/', views.signup , name='signup'),
    path('signin/', views.signin , name='signin'),
    path('logout/', views.logout_ , name='logout'),
    path('insert-page-id/', views.InsertPageId , name='insertPageId'),
    path('<int:pageId>/edit-page-id/', views.EditPageId , name='EditPageId'),
    # path("login/", views.login, name="login"),


]