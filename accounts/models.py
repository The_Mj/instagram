from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.

class Info (models.Model):
    user = models.ForeignKey(User , on_delete=models.CASCADE)
    pageId = models.CharField(max_length=128 )


# @receiver(post_save, sender=User)
# def create_user_info(sender, instance, created, **kwargs):
#     if created:
#         Info.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def save_user_info(sender, instance, **kwargs):
#     # instance.info.save()
#     info =Info.objects.filter(user=instance)
#     for i in info:
#         i.user.save()
