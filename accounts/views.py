from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse, HttpResponse , HttpResponseRedirect
import requests
from django.contrib.auth import get_user_model
User = get_user_model()
from accounts.models import Info
# Create your views here.


def signup(request):
    error_pageId = False
    error_pass = False
    error_user = False
    userToken = 'EAAn2JFZAw9GEBAPnXBJ0ZClZB94Bk7VEnDsy7h558XQ7mjNDNcZCThXSre34ppwgmxUtW5BS7ozLLU3313anKRXRvungalWIlZBUnEpzgS2mShmTvsZBA1OHX2CNzMUYfv8HfSjLZBBZAMFZA4YzkLsDjIDuPKUhegTSqKFUWb4ceTwZDZD'
    if request.method == 'POST':
        print('s1')
        pageId = str(request.POST.get("pageId"))
        first_name = request.POST.get("first_name")
        last_name = request.POST.get("last_name")
        username = request.POST.get("username")
        email = request.POST.get("email")
        pageIdapi = requests.get('https://graph.facebook.com/v8.0/me?fields=id%2Cname%2Caccounts&access_token={}'.format(userToken))


        for i in pageIdapi.json()['accounts']['data']:
            print(i)
            if i['id'] == pageId:
                print(i['id'])
                password = request.POST.get("password1")
                password_repeat = request.POST.get("password2")
                if password != password_repeat:
                    error_pass = True
                    return render(request, "signup.html", {"error_pass": error_pass})
                if User.objects.filter(username=username).exists() :
                    error_user = True
                    return render(request, "signup.html", {"error_user": error_user})
                user = User.objects.create_user(username=username,first_name=first_name, last_name=last_name, email=email )
                # user.Info.pageId=pageId
                info=Info(user=user , pageId=pageId)
                info.save()
                user.set_password(password)
                user.save()

                return HttpResponseRedirect("/")
        else:
            error_pageId = True
            return render(request, "Register.html", {"error_pageId": error_pageId})
    return render(request, "Register.html")


def logout_(request):
    logout(request)
    return HttpResponseRedirect("/")


def signin(request):
    error = False
    if request.method == 'POST':
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect("/")
        elif user is None:
            error = True
    return render(request, "login.html", {
        "error": error
    })

def InsertPageId(request):
    pageIdlist= []
    if request.method == 'POST':
        print(type(request.POST))

        for x in range(14):
            if request.POST.get('mytext[{}]'.format(x)) != None:
                pageIdlist.append(request.POST.get('mytext[{}]'.format(x)))
        for i in pageIdlist:
            info = Info.objects.create(user=request.user , pageId=i)
            info.save()

        return HttpResponseRedirect("/")
    return render(request , 'pageId.html')

def EditPageId(request , pageId):
    # info = Info.objects.filter(user=request.user)
    if request.method == 'POST':
        request.POST._mutable= True
        # print(request.POST._mutable)
        newpageId=request.POST.pop('pageId')

        info = Info.objects.get(user=request.user , pageId=pageId)
        info.pageId=newpageId
        info.save()
        return HttpResponseRedirect('/')

    return render(request , 'EditPageId.html' , { 'pageId' : pageId })


